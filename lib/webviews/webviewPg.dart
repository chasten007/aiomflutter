import 'dart:async';

import 'package:android_intent/android_intent.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class webviewPg extends StatefulWidget {
  final String title;
  final String url;
  final String playstoreUrl;

  webviewPg({
    @required this.title,
    @required this.url,
    @required this.playstoreUrl,
  });

  @override
  State<StatefulWidget> createState() {
    return webviewState(title1: title, url1: url, playstoreUrl1: playstoreUrl);
  }
}

class webviewState extends State<webviewPg> {
  final String title1;
  final String url1;
  final String playstoreUrl1;
  final Completer<WebViewController> _controleeer =
      Completer<WebViewController>();
  num position = 1;

  final key = UniqueKey();

  doneLoading(String A) {
    setState(() {
      position = 0;
    });
  }

  startLoading(String A) {
    setState(() {
      position = 1;
    });
  }

  webviewState({
    @required this.title1,
    @required this.url1,
    @required this.playstoreUrl1,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title1),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.shop),
              onPressed: () {
                _lauchPlaystore(playstoreUrl1);
              },
            ),
          ],
        ),
        body: IndexedStack(index: position, children: <Widget>[
          WebView(
            initialUrl: url1,
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webcontroller) {
              _controleeer.complete(webcontroller);
            },
            key: key,
            onPageFinished: doneLoading,
            onPageStarted: startLoading,
          ),
          Container(
            color: Colors.white,
            child: Center(child: CircularProgressIndicator()),
          ),
        ])
//      body: WebView(
//        initialUrl: url1,
//        javascriptMode: JavascriptMode.unrestricted,
//        onWebViewCreated: (WebViewController webcontroller) {
//          _controleeer.complete(webcontroller);
//        },
//      ),
        );
  }
}

void _lauchPlaystore(String playstoreUrl) {
//  if (platform.isAndroid) {
  AndroidIntent intent = new AndroidIntent(
      action: 'action_view',
      data: Uri.encodeFull(playstoreUrl),
      package: 'com.android.vending'
//      arguments: {'authAccount': currentUserEmail},
      );
  intent.launch();
//  }
}
