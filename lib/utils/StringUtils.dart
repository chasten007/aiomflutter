import 'package:shared_preferences/shared_preferences.dart';

class StringUtils {
  static const String appName = "AIOM";

  static const String aboutus = "About us";
  static const String contactus = "Contact us";

  static String KeyFavAPP = "Key_App";
  static String KeyFavAPPIcon = "Key_Icon";

  static List<String> favApps;
  static List<String> favAppsImg;

  static List<String> tops = [
    "Amazon",//1
    "Flipkart",//2
    "Ajio",//3
    "Hotstar",//4
    "Netflix",//5
    "Myntra",//6
    "Phonepe",//7
    "Patanjali",//8
    "GooglePay",//9
    "Paytm",//10
    "PharmEasy",//11
    "AmazonKindle",//12
    "TOI",//13
    "Gaana",//14
    "Pepperfry",//15
    "OLX",//16
    "MakeMyTrip",//17  //add irctc
    "OLA",//18
    "GMAIL",//19
    "Instagram",//20
    "just dial"//21
  ];
  static List<String> topsPlaystoreUrl = [
    "https://play.google.com/store/apps/details?id=in.amazon.mShop.android.shopping&hl=en_IN",
    "Flipkart",
    "Ajio",
    "Hotstar",
    "Netflix",
    "Myntra",
    "Phonepe",
    "Patanjali",
    "GooglePay",
    "Paytm",
    "PharmEasy",
    "AmazonKindle",
    "TOI",
    "Gaana",
    "Pepperfry",
    "OLX",
    "MakeMyTrip", //add irctc
    "OLA",
    "GMAIL",
    "Instagram",
    "just dial"
  ];

  static List<String> topsImgs = [
    "https://lh3.googleusercontent.com/TpxeOgptQworHjromT4yoo05oHk72dSE0pVQowPfb3u3rozTwLjYUHxTpEzIYMemomQ=s180-rw",//1
    "https://lh3.googleusercontent.com/q8hxnbpJCYfHipSOG_5tZe5jK_89T6QIsqrEklvGpMFKH8b98pDHJf2tPcn2bxEN96ON=s180-rw",//2
    "https://lh3.googleusercontent.com/RWNQyHoMPJ-Z8ApQhQchXsfoBXrj3By1cf49GPRK6-hYiIv0RL6Z1fdZl1OAUpqHCB8=s180-rw",//3
    "https://lh3.googleusercontent.com/MmLkAp-x9OvA46_NgaD7dpXIsPkvb0OTJ-WlK_-7vyjZMjBMgJ0zHhsgg2NI3r0Lobc=s180-rw",//4
    "https://lh3.googleusercontent.com/TBRwjS_qfJCSj1m7zZB93FnpJM5fSpMA_wUlFDLxWAb45T9RmwBvQd5cWR5viJJOhkI=s180-rw",//5
    "https://lh3.googleusercontent.com/G87iT9dhwxwtgl5UA8SIqwp7I0GTlItyuR5mNL7vUOgP2SN8JyXigaiOpxKRaCXI22w=s180-rw",//6
    "https://lh3.googleusercontent.com/6iyA2zVz5PyyMjK5SIxdUhrb7oh9cYVXJ93q6DZkmx07Er1o90PXYeo6mzL4VC2Gj9s=s180-rw",//7
    "https://lh3.googleusercontent.com/DDlAk8C5j1QqfGV5St_Xfxbfd6ci-Z17HnW5OyvXq63-WbcKAGW5DEM0Z8KFH-8GJX0=s180-rw",//8
    "https://lh3.googleusercontent.com/AeMKuV3iGZsHPeSU_g13oYW0msutmjt3QiEbJvTiMh6dqFvyeTS-LHVs4Sa0d9q7RElI=s180-rw",//9
    "https://lh3.googleusercontent.com/k7yz57K2OxhNrPNKF2U18Zcv9rodOu7CfWh47U15FFUN8-_B0hQfXsM-BaLG0gOtvw=s180-rw",//10
    "https://lh3.googleusercontent.com/94w8YgbYtWqoaZsg1ROsnEhNlAOaEL376bIoiN36jNN4UKnF2hchJdaLSksKP_38bMs=s180-rw",//11
    "https://lh3.googleusercontent.com/HFQH-DmzwCzgtiUisd_S-mPbUE7bfXRDyXUBEIBrM9y_nHbmEoQXz94BBHErEyOmPg=s180-rw",//12
    "https://lh3.googleusercontent.com/fUYvwMn3wdrIUAwkG-2b8mHa3OeJEmtHl0vQIdHZqkIxuCuU6W_Xvlql9ySqvjnNQ0Y=s180-rw",//13
    "https://lh3.googleusercontent.com/vHw1Qv2MNAzoXiuJb8lNkybyHBzCsiWblKCefKnsukJlV9z4G0hGL_4uXzLUwxyT7a_q=s180-rw",//4
    "https://lh3.googleusercontent.com/DRUl_Itt6wKtdfGRAOuSRHaXz9pVhNEgS1C-f2jylSbDMZxh_PNyRO2ZdnY-kCcznnoC=s180-rw",//5
    "https://lh3.googleusercontent.com/tYdcwxICaq7q-Qe_jmQ-2YIMfZ011Tap8PtWLOOpLx23LJLvqr_YziUqek9nBEdVJE8=s180-rw",//6
    "https://lh3.googleusercontent.com/1llAcleLs0UDr5ysUl_C4aHM-vN70HGTy7gjlM78SLsPCOijj7oosBLQc26G2daqOg=s180-rw",//7
    "https://lh3.googleusercontent.com/4rrRCFJo4GSxHBON0M8OIwKNfT6b5zOA20fMniJKaeU3CUylFA9i6vazeazxKwZU6Ng=s180-rw",//8
    "https://lh3.googleusercontent.com/qTG9HMCp-s_aubJGeQWkR6M_myn-aXDJnraWn9oePcY1dGbYqXibaeLQBAeMdmxSBus=s180-rw",//9
    "https://lh3.googleusercontent.com/c2DcVsBUhJb3UlAGABHwafpuhstHwORpVwWZ0RvWY7NPrgdtT2o4JRhcyO49ehhUNRca=s180-rw",//20
    "https://lh3.googleusercontent.com/F6SHkgLMR6vIfIKqs13pKKrPOkdkcACkVPougKAhI31C5XmbECB-WsTX36pRBbv-aMXQ=s180-rw"//21
  ];

  static List<String> topAppsUrls = [
    "https://www.amazon.in/",//1
    "https://www.flipkart.com/",//2
    "https://www.ajio.com/",//3
    "https://www.hotstar.com/in",//4
    "https://www.netflix.com/in/",//5
    "https://www.myntra.com/",//6
    // 7 noweburl
    "https://play.google.com/store/apps/details?id=com.phonepe.app&hl=en_IN",//7
    "https://www.patanjaliayurved.net/",//8
    // 9 no
    "https://play.google.com/store/apps/details?id=com.google.android.apps.nbu.paisa.user",//9
    "https://paytm.com/",//10
    "https://pharmeasy.in/",//11
    "https://www.amazon.in/Kindle-eBooks/b?ie=UTF8&node=1634753031",//12
    "https://timesofindia.indiatimes.com/",//13
    "https://gaana.com/",//14
    "https://www.pepperfry.com/",//15
    "https://www.olx.in/",//16
    "https://www.makemytrip.com/",//17 //add irctc
    "https://www.olacabs.com/",//18
    "https://mail.google.com/mail/u/0/#inbox",//19
    "https://www.instagram.com/?hl=en",//20
    "https://www.justdial.com/"//21
  ];

  static List<String> clothingsShopping = [
    "Amazon",
    "AJIO",
    "Flipkart",
    "LimeRoad",
    "Lenskart",
    "Tata CLiQ",
    "ShopClues",
    "Snapdeal",
    "Myntra",
    "Nykaa",
    "FirstCry"
  ];
  static List<String> clothingsShoppingUrl = [
    "https://www.amazon.in/Clothing-accesories/b?ie=UTF8&node=1571271031",
    "https://www.ajio.com/",
    "https://www.flipkart.com/clothing-and-accessories/pr?sid=clo",
    "https://www.limeroad.com/",
    "https://www.lenskart.com/",
    "https://www.tatacliq.com/",
    "https://www.shopclues.com/",
    "https://www.snapdeal.com/",
    "https://www.myntra.com/",
    "https://www.nykaa.com/",
    "https://www.firstcry.com/"
  ];
  static List<String> clothingIcons = [
    "https://lh3.googleusercontent.com/TpxeOgptQworHjromT4yoo05oHk72dSE0pVQowPfb3u3rozTwLjYUHxTpEzIYMemomQ=s180-rw",
    "https://lh3.googleusercontent.com/RWNQyHoMPJ-Z8ApQhQchXsfoBXrj3By1cf49GPRK6-hYiIv0RL6Z1fdZl1OAUpqHCB8=s180-rw",
    "https://lh3.googleusercontent.com/q8hxnbpJCYfHipSOG_5tZe5jK_89T6QIsqrEklvGpMFKH8b98pDHJf2tPcn2bxEN96ON=s180-rw",
    "https://lh3.googleusercontent.com/G2KkRGnyD0os9HYYc2c3FWS57KNQRdTJSDUKWkSzX5q4OnSy9XNnXXlxmNUI_j4uwg=s180-rw",
    "https://lh3.googleusercontent.com/wPGVqEoS2gtvGLHPaifWXPy53S-5-zjq_Cl5YBhpAVQ_FtQ0gicsKmOCgQrg1yQIzus=s180-rw",
    "https://lh3.googleusercontent.com/YYMB0q3VZuH1cegb-s636R6p4cIPivJ-4BMn04ogVWfw1l0JrJSFYZ2xvcjsPEWY5ro=s180-rw",
    "https://lh3.googleusercontent.com/oaOz1gpJtbH4NiD0qjNBN-TjNIkR7jKkhbmN5vHVwQcVHJhSswoXn3IffwY6EUde5H0k=s180-rw",
    "https://lh3.googleusercontent.com/TOx0bRExNwYQr-0jgzBRtGGLkOrcaBmLq0xzJT4faABxHFZ0wo9_WoMorx1ubcl_yn4=s180-rw",
    "https://lh3.googleusercontent.com/G87iT9dhwxwtgl5UA8SIqwp7I0GTlItyuR5mNL7vUOgP2SN8JyXigaiOpxKRaCXI22w=s180-rw",
    "https://lh3.googleusercontent.com/RllZf42XounX60rftJqa0K3EfoCMmzGqdOgbZvtQiPETcOVaes8cw1bEeBhW7UGeNb0=s180-rw",
    "https://lh3.googleusercontent.com/MnuZ2c-iJgXdlyjs1lMAzBR6meMfkSBmPAebq9LGnVi83NNGpd0dPMGS5sJQS_EO_w=s180-rw"
  ];

  static List<String> grocery = ["Bigbasket", "DMart", "Grofers", "Patanjali"];
  static List<String> groceryUrls = [
    "https://www.bigbasket.com/",
    "https://www.dmartindia.com/",
    "https://grofers.com/",
    "https://www.patanjaliayurved.net/",
  ];
  static List<String> groceryIcon = [
    "https://lh3.googleusercontent.com/EuiZnkT8aEKjXDLX74DTp1VRIwWaeRa8Dvo-LOGAxy1FPQ8GzABTIRenksiM-A7Oz48g=s180-rw",
    "https://lh3.googleusercontent.com/0FxNc84rRnHI3dBapmW6fi6AnJnRS3QrXoCDO3WolHso1-cgeocX_iikjUbsMI0doco=s180-rw",
    "https://lh3.googleusercontent.com/dfUJZCIJhu6AGl4HzjZEKHxvX8j7KcN67lAgj-B4pcgT_4tO7PlbBb5BehEn1B2x6hM=s180-rw",
    "https://lh3.googleusercontent.com/DDlAk8C5j1QqfGV5St_Xfxbfd6ci-Z17HnW5OyvXq63-WbcKAGW5DEM0Z8KFH-8GJX0=s180-rw"
  ];

  static List<String> newsNentertainment = [
    "Inshorts",
    "TOI",
    "Weather forecast",
    "Cricbuzz",
    "The Hindu",
    "Bhaskar",
    "Dailyhunt",
    "Gaana",
    "BookMyShow",
    "JioSaavn",
    "Wynk",
    "spotify",
    "Zee5",
    "Hotstar",
    "ALT",
    "Amazon Prime",
    "TVF",
    "Netflix",
    "Voot",
    "JioCinema",
    "SonyLiv"
  ];
  static List<String> newsNentertainmentUrls = [
    "https://inshorts.com/en/read",
    "https://timesofindia.indiatimes.com/",
    "https://www.accuweather.com/en/in/ahmedabad/202438/weather-forecast/202438",
    "https://www.cricbuzz.com/",
    "https://www.thehindu.com/",
    "https://www.bhaskar.com/",
    "https://m.dailyhunt.in/news/",
    "https://gaana.com/",
    "https://in.bookmyshow.com/",
    "https://www.jiosaavn.com/",
    "https://wynk.in/music",
    "https://www.spotify.com/in/",
    "https://www.zee5.com/",
    "https://www.hotstar.com/in",
    "https://www.altbalaji.com/",
    "https://www.primevideo.com/?ref_=dvm_pds_amz_in_as_s_g_brand1|m_lgAX6a65c_c386561644245",
    "https://tvfplay.com/",
    "https://www.netflix.com/in/",
    "https://www.voot.com/",
    "https://www.jiocinema.com/",
    "https://www.sonyliv.com/"
  ];
  static List<String> newsNentertainmentIcons = [
    "https://lh3.googleusercontent.com/lvAGNNFIBMC3ZgS-JJcSwDzpSjOGMg7FGY14FrffJobLOed9-H2DiuZwknsIwVwKwTjo=s180-rw",
    "https://lh3.googleusercontent.com/fUYvwMn3wdrIUAwkG-2b8mHa3OeJEmtHl0vQIdHZqkIxuCuU6W_Xvlql9ySqvjnNQ0Y=s180-rw",
    "https://lh3.googleusercontent.com/L88OtVQ78D-XWBbBHtPF1n48Ej8DZOI_6eGULrh1oBlIwNsoaCe6CzPVWIcWhEDIPEk=s180-rw",
    "https://lh3.googleusercontent.com/3z09fC48t4igPsjnohFu1Zbm1POROtwBRR4pAC4ZpDR5ZCyf0xIx3e3oHAZjdhivgQ=s180-rw",
    "https://lh3.googleusercontent.com/ygazxlwV00_yT1bg2Yi1MthMjPMPe9XbA2AxZb9iL9N85vA2QcQ-vzqYRD19TVyEgoXG=s180-rw",
    "https://lh3.googleusercontent.com/oxDyxSyoNefbm3x8qC4TVgfk1aT2CQu1xQErXfQxkU_y6NmYxz4b0rvn4QWVr1Artw4=s180-rw",
    "https://lh3.googleusercontent.com/Ml7kliqttZzEpXZTHty2LOGyt612poDQ1QoEL6QBoMsK1JyZHlyStKCrjhAxW8Z6e8Nj=s180-rw",
    "https://lh3.googleusercontent.com/vHw1Qv2MNAzoXiuJb8lNkybyHBzCsiWblKCefKnsukJlV9z4G0hGL_4uXzLUwxyT7a_q=s180-rw",
    "https://lh3.googleusercontent.com/KSXq3Idt1WbHnaHcsC4p5KxaRE0KuuB-GNRJP3XRS2omj6DY8ohkbMlIbK53Sp6TlxYm=s180-rw",
    "https://lh3.googleusercontent.com/FWHOs52Nk7BJi_FhwJFhuZpijl9L_A2GxwE7y62hhOeHNZUP5z2jrWgHp4tdKf8w06U=s180-rw",
    "https://lh3.googleusercontent.com/WsLGolbognnEQ5OZyaQmwnPBRPqreFMbuyVemHEKQwgvBJBfupxigoecBydCXes4ciHS=s180-rw",
    "https://lh3.googleusercontent.com/UrY7BAZ-XfXGpfkeWg0zCCeo-7ras4DCoRalC_WXXWTK9q5b0Iw7B0YQMsVxZaNB7DM=s180-rw",
    "https://lh3.googleusercontent.com/U4_8eXv9G0m3zkxYGsC85ZxfXecg9q_zd6cBhGzCCt973bMC534SpQ2iQS5slzqc1Lrh=s180-rw",
    "https://lh3.googleusercontent.com/MmLkAp-x9OvA46_NgaD7dpXIsPkvb0OTJ-WlK_-7vyjZMjBMgJ0zHhsgg2NI3r0Lobc=s180-rw",
    "https://lh3.googleusercontent.com/LorUdiKkD2jgnPzJGaExsp8sBN_njftCz5GYKRSpQqj1csHYNQnBV7NWvQ-6l4FZZQ=s180-rw",
    "https://lh3.googleusercontent.com/8KcvptOGk9PE0X-i5mxMeUxnSpL7IkpAP_kApqLHRbkOeYTe-jKPx_wu3_FaIkL0W1M=s180-rw",
    "https://lh3.googleusercontent.com/eZXe8deu-WLlePh33G1-FPZDqkin7yJsH_z_4fUTlY6O_uEcky6WBpoFXLqwe3b_7nk=s180-rw",
    "https://lh3.googleusercontent.com/TBRwjS_qfJCSj1m7zZB93FnpJM5fSpMA_wUlFDLxWAb45T9RmwBvQd5cWR5viJJOhkI=s180-rw",
    "https://lh3.googleusercontent.com/xG9e5xK60y6j9Fj-i5Z_055tYYyVnY3pBRA83sieC5QLR3lMLN6tf-8h5E-Rb5gs7wo=s180-rw",
    "https://lh3.googleusercontent.com/LuruMgZZ3D08QNjUzUePG6C55PCkk6tgCNwCaeNKu3K4nKioQKGxKvxx-mUt8f6syw=s180-rw",
    "https://lh3.googleusercontent.com/QtxELma_6y1jezI8QKqVtI8Tb0flMhPjIAzU-VSZ2jz7RwriCENXQk4M6MvUyVi5qg=s180-rw"
  ];

  static List<String> mobElect = [
    "Amazon",
    "Flipkart",
    "Mobile Price Comparison"
  ];
  static List<String> mobElectUrl = [
    "https://www.amazon.in/mobile-phones/b?ie=UTF8&node=1389401031",
    "https://www.flipkart.com/mobile-phones-store",
    "Mobile Price"
  ];

  static List<String> mobIcons = [
    "https://lh3.googleusercontent.com/TpxeOgptQworHjromT4yoo05oHk72dSE0pVQowPfb3u3rozTwLjYUHxTpEzIYMemomQ=s180-rw",
    "https://lh3.googleusercontent.com/q8hxnbpJCYfHipSOG_5tZe5jK_89T6QIsqrEklvGpMFKH8b98pDHJf2tPcn2bxEN96ON=s180-rw",
    "https://lh3.googleusercontent.com/YjO3MoN7jv4_OLJGzR4tiekDuJD5Hu-U7D5JTKPz_KdLb2dirsVt3KTKBe2gZpRFtR8=s180-rw"
  ];

  static List<String> homeDecoreFurniture = [
    "RoomPlanner",
    "Pepperfry",
    "Amazon",
    "Flipkart",
    "MagicBricks",
    "99acers"
  ];
  static List<String> homeDecoreFurnitureUrls = [
    "https://www.roomplannerapp.com/",
    "https://www.pepperfry.com/",
    "https://www.amazon.in/Furniture/b?ie=UTF8&node=1380441031",
    "https://www.flipkart.com/furniture-store",
    "https://www.magicbricks.com/",
    "https://www.magicbricks.com/"
  ];
  static List<String> homeIcons = [
    "https://lh3.googleusercontent.com/e6kUFH_MspS1SZbCUDGVYnifmYmGkS9GnjD59NTcREArZ3WezoalwxJolzzpqLyZFUQ=s180-rw",
    "https://lh3.googleusercontent.com/DRUl_Itt6wKtdfGRAOuSRHaXz9pVhNEgS1C-f2jylSbDMZxh_PNyRO2ZdnY-kCcznnoC=s180-rw",
    "https://lh3.googleusercontent.com/TpxeOgptQworHjromT4yoo05oHk72dSE0pVQowPfb3u3rozTwLjYUHxTpEzIYMemomQ=s180-rw",
    "https://lh3.googleusercontent.com/q8hxnbpJCYfHipSOG_5tZe5jK_89T6QIsqrEklvGpMFKH8b98pDHJf2tPcn2bxEN96ON=s180-rw",
    "https://lh3.googleusercontent.com/Yf59nDAW7WZp-VRsUeEVC3CtpWy9lnF__Y-gMqEzRxbLpG8ga69DPHUkkQaHqYz9jbA=s180-rw",
    "https://lh3.googleusercontent.com/0c3VhbTRqGPb9AH_rhScqpfXTcSk9KZRlgf9dveHVITv-AWmv7Rok88qGyHuLas2lA=s180-rw"
  ];

  static List<String> books = ["Wikipedia", "AmazonKindle", "PocketBook"];
  static List<String> booksUrls = [
    "https://www.wikipedia.org/",
    "https://www.amazon.in/Kindle-eBooks/b?ie=UTF8&node=1634753031",
    "PocketBook"
  ];
  static List<String> booksIcons = [
    "https://lh3.googleusercontent.com/htBUaqvBQR9UQ3b1-ouSHFhDGttQkH-eWetEErspYXVa8hOsfmOmj5ZanGg9GF7XAGc=s180-rw",
    "https://lh3.googleusercontent.com/HFQH-DmzwCzgtiUisd_S-mPbUE7bfXRDyXUBEIBrM9y_nHbmEoQXz94BBHErEyOmPg=s180-rw",
    "https://lh3.googleusercontent.com/XC4XyY4hCFcab0EwMO6_QFWVJboY0ADSM-CiEWAQCZ2VJ5qLL1ABcsFXE1z6Kk-Epbw=s180-rw"
  ];

  static List<String> vehicles = [
    "Quikr",
    "CarDekho",
    "Vehicle Owner Details",
    "OLX",
    "mParivahan"
  ];
  static List<String> vehiclesUrl = [
    "https://www.quikr.com/cars-bikes/ahmedabad+w160",
    "https://www.cardekho.com/",
    "Vehicle Owner Details",
    "https://www.olx.in/",
    "https://parivahan.gov.in/parivahan//en/content/mparivahan"
  ];
  static List<String> vehiclesIcons = [
    "https://lh3.googleusercontent.com/l5vy1ANtkBFog57kFEGOtPKYH7HDhnUFePivRBa3LbyzgqohMGEMfQOkYCRu-gQgRQ=s180-rw",
    "https://lh3.googleusercontent.com/q-Rb7P_S9tEdK1gwW62vJmPI8H10CZruS9Ne14YMZXLXCeHH2mB_XYMuItqauywlFmIc=s180-rw",
    "https://lh3.googleusercontent.com/NOyyusXmKpxEAjZyoDdRFvESE1JEPdxGL14j32UV5sRjZ-hyEuxmzb9RyB4XMbkQmA=s180-rw",
    "https://lh3.googleusercontent.com/tYdcwxICaq7q-Qe_jmQ-2YIMfZ011Tap8PtWLOOpLx23LJLvqr_YziUqek9nBEdVJE8=s180-rw",
    "https://lh3.googleusercontent.com/YqOG9GBAB3n9Cw9NbkdlgcV8H1UuxqtotohizT8BjFK8QWVgjSEoEO1Gr-AyJMg5Tw=s180-rw"
  ];

  static List<String> paymentUpi = [
    "Phonepe",
    "GooglePay",
    "paypal",
    "AmazonPay",
    "Paytm",
    "Freecharge"
  ];
  static List<String> paymentUpiUrl = [
    "Phonepe",
    "GooglePay",
    "https://www.paypal.com/",
    "https://www.amazon.in/amazonpay/home?ref_=apay_logo_APayDashboard",
    "https://paytm.com/",
    "https://www.freecharge.in/"
  ];
  static List<String> paymentIcon = [
    "https://lh3.googleusercontent.com/6iyA2zVz5PyyMjK5SIxdUhrb7oh9cYVXJ93q6DZkmx07Er1o90PXYeo6mzL4VC2Gj9s=s180-rw",
    "https://lh3.googleusercontent.com/AeMKuV3iGZsHPeSU_g13oYW0msutmjt3QiEbJvTiMh6dqFvyeTS-LHVs4Sa0d9q7RElI=s180-rw",
    "https://lh3.googleusercontent.com/Y2_nyEd0zJftXnlhQrWoweEvAy4RzbpDah_65JGQDKo9zCcBxHVpajYgXWFZcXdKS_o=s180-rw",
    "https://lh3.googleusercontent.com/TpxeOgptQworHjromT4yoo05oHk72dSE0pVQowPfb3u3rozTwLjYUHxTpEzIYMemomQ=s180-rw",
    "https://lh3.googleusercontent.com/k7yz57K2OxhNrPNKF2U18Zcv9rodOu7CfWh47U15FFUN8-_B0hQfXsM-BaLG0gOtvw=s180-rw",
    "https://lh3.googleusercontent.com/9MgLa0ekOqhj_OosRcJis7O6Y20ckxK7T0gMnIQ2fnhauoT-UZSJqHk2uRoED2eeOA=s180-rw"
  ];

  static List<String> travelNFood = [
    "OLA",
    "UBER",
    "OYO",
    "MakeMyTrip",
    "Goibibo",
    "IRCTC",
    "Redbus",
    "Zomoto",
    "Swiggy",
    "UberEats",
  ];
  static List<String> travelNFoodUrl = [
    "https://www.olacabs.com/",
    "https://www.uber.com/in/en/",
    "https://www.oyorooms.com/",
    "https://www.makemytrip.com/",
    "https://www.goibibo.com/",
    "https://www.irctc.co.in/nget/",
    "https://www.redbus.com/",
    "https://www.zomato.com/",
    "https://www.swiggy.com/",
    "https://www.ubereats.com/",
  ];
  static List<String> travelNFoodIcons = [
    "https://lh3.googleusercontent.com/4rrRCFJo4GSxHBON0M8OIwKNfT6b5zOA20fMniJKaeU3CUylFA9i6vazeazxKwZU6Ng=s180-rw",
    "https://lh3.googleusercontent.com/qy_wZ92sFQccojEtscg52vtdAQmCIeQ4jsybMPmuML9Or7_SEOyrt0Jn0wyG-l2Fyw=s180-rw",
    "https://lh3.googleusercontent.com/hXpVzlM5gVL6hMCLacy3Y9zd2ecDVlU7TsHgSDB1R9u8OjUCUJ1bCrsUsT4bP-PyCSo=s180-rw",
    "https://lh3.googleusercontent.com/1llAcleLs0UDr5ysUl_C4aHM-vN70HGTy7gjlM78SLsPCOijj7oosBLQc26G2daqOg=s180-rw",
    "https://lh3.googleusercontent.com/tBgDkTPpodjVh4OYlMgrmxRUY4PZuuJUiD4F4IqsqlCfAA5YScwpd3LjUyVmfyfH3A=s180-rw",
    "https://lh3.googleusercontent.com/W-dIYizhlZulse5gtu7IDhTHgFW7DrDMJy-o5T0FdPBI4k4MY2aDW1MAfMcZZVwUtQ=s180-rw",
    "https://lh3.googleusercontent.com/5ZxVI65M9_yQQHgsY2f_lvSFD9E4Oqvfgxkg-E-MZwWt1M65-6HLY3twREAubQtZqqo=s180-rw",
    "https://lh3.googleusercontent.com/bk1WWaf0R9NpyYleMlg9C8ttglyT0T61jqAfrNylBRMPorCVh-QuGjxTc1inAPIxnQ=s180-rw",
    "https://lh3.googleusercontent.com/A8jF58KO1y2uHPBUaaHbs9zSvPHoS1FrMdrg8jooV9ftDidkOhnKNWacfPhjKae1IA=s180-rw",
    "https://lh3.googleusercontent.com/MMBG4AZmpMhSfhF5k7QnFmhvFbaF5ZC_BtEOIKRt9TIkUZjul2lWwPZV75PwTfoSm23-jgMxkroRGA-vkDg=s180-rw",
  ];

  static List<String> socialNetwork = [
    "Facebook",
    "Gmail",
    "Instagram",
    "Yahoo",
    "Twitter",
    "LinkedIn",
    "Pinterest"
  ];
  static List<String> socialNetworkUrls = [
    "https://www.facebook.com/",
    "https://mail.google.com/mail/u/0/#inbox",
    "https://www.instagram.com/?hl=en",
    "https://in.yahoo.com/",
    "https://twitter.com/",
    "https://www.linkedin.com/",
    "https://in.pinterest.com/"
  ];
  static List<String> socialNetworkIcons = [
    "https://lh3.googleusercontent.com/UwKAdIulEU3eKrqbR5lTngf_D3NDennsPGzmmtS2-pIqdhYXs5D-TvT9htpVRIh6gQ=s180-rw",
    "https://lh3.googleusercontent.com/qTG9HMCp-s_aubJGeQWkR6M_myn-aXDJnraWn9oePcY1dGbYqXibaeLQBAeMdmxSBus=s180-rw",
    "https://lh3.googleusercontent.com/c2DcVsBUhJb3UlAGABHwafpuhstHwORpVwWZ0RvWY7NPrgdtT2o4JRhcyO49ehhUNRca=s180-rw",
    "https://lh3.googleusercontent.com/-4_YrKdlxuAKrWO3HvMtBIEoucEX2OMldYg5CfM0tBygzbc96JOm26v9ZDUmtcG4Nw=s180-rw",
    "https://lh3.googleusercontent.com/wIf3HtczQDjHzHuu7vezhqNs0zXAG85F7VmP7nhsTxO3OHegrVXlqIh_DWBYi86FTIGk=s180-rw",
    "https://lh3.googleusercontent.com/fqYJHtyzZzA4vacRzeJoB93QNvA5-mvR-8UB5oVLxdYDSTpfLp_KgYD4IqVGJUgFEJo=s180-rw",
    "https://lh3.googleusercontent.com/dVsv8Hc4TOUeLFAahxR8KANg22W9dj2jBsTW1VHv3CV-5NCZjP9D9i2j5IpfVx2NTB8=s180-rw"
  ];

  static List<String> other = [
    "Justdial",
    "Urban Company",
    "Google Fit",
    "PharmEasy",
    "Netmeds"
  ];
  static List<String> otherUrl = [
    "https://www.justdial.com/",
    "https://www.urbancompany.com/",
    "Google Fit",
    "https://pharmeasy.in/",
    "https://www.netmeds.com/"
  ]; //torch
  static List<String> otherIcons = [
    "https://lh3.googleusercontent.com/F6SHkgLMR6vIfIKqs13pKKrPOkdkcACkVPougKAhI31C5XmbECB-WsTX36pRBbv-aMXQ=s180-rw",
    "https://lh3.googleusercontent.com/W6nw7wKPPlalpwHhP-qOSqoiFeVwATDKxbTj6QUm6VIJjyTS-Uq_aqvRP8-1f65z7c4=s180-rw",
    "https://lh3.googleusercontent.com/jArSD-kxOa2llPXvqrjRcEJdL4XhjP8-WqEfg9UAlYF8v0qzXAZ0EI5k96l0pf3tDNg=s180-rw",
    "https://lh3.googleusercontent.com/k7yz57K2OxhNrPNKF2U18Zcv9rodOu7CfWh47U15FFUN8-_B0hQfXsM-BaLG0gOtvw=s180-rw",
    "https://lh3.googleusercontent.com/Eq9ne7fk6fbs_fpUU1Z_EuowRThYOWHx_BPnoKfHmiv6Tpa2CH1Ia_hPMAMV7useig=s180-rw"
  ];

  static String aboutUsMessage =
      "This app is totally informative app! we are not promoting any one neither with our choice nor with rs i.e its not paid promotion!Please note that this app is according to market and user needs and their recommendation only! Hope you all like AIOM";
  static String contactUs =
      "If you have any suggestion or issue please feel free to contact us on given email. We are eager to hear from you!";
}
