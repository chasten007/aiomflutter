import 'package:AllInOneMall/settings/FavAppList.dart';
import 'package:AllInOneMall/utils/ScaleRoute.dart';
import 'package:AllInOneMall/utils/StringUtils.dart';
import 'file:///D:/projects/allinonemall/lib/webviews/webviewPg.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:shared_preferences/shared_preferences.dart';

class widgetHelper {

  static Widget gridlist(BuildContext context, List<String> list,
      List<String> listIcons, List<String> weburls, List<String> playstoreUrl) {
    Widget gridlist = AnimationLimiter(
        child: GridView.count(
      crossAxisCount: 2,
      crossAxisSpacing: 15.0,
      mainAxisSpacing: 15.0,
      shrinkWrap: false,
      children: List.generate(list.length, (index) {
        return AnimationConfiguration.staggeredGrid(
          columnCount: 3,
          position: index,
          duration: const Duration(milliseconds: 375),
          child: ScaleAnimation(
            scale: 0.5,
            child: FadeInAnimation(
              child: GestureDetector(

                onTap: () {
                  Navigator.push(
                      context,
                      ScaleRoute(
                          page: webviewPg(
                        title: list[index],
                        url: weburls[index],
                        playstoreUrl: playstoreUrl[index],
                      )));
                },
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                  elevation: 5.0,
                  child: Container(
                    margin: EdgeInsets.only(top: 10.0),
                    child: Column(
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          child: Image.network(listIcons[index],
                              // width: 300,
                              height: 117.5,
                              fit: BoxFit.fitWidth),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 22.5),
                          height: 30,
                          width: double.infinity,
                          color: Colors.blueAccent,
                          alignment: Alignment.center,
                          child: Text(
                            list[index],
                            maxLines: 1,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold),
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      }),
    ));
    return gridlist;
  }



}
