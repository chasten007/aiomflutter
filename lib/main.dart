import 'package:AllInOneMall/HomeScreen.dart';
import 'package:flutter/material.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: SplashScreen());
  }
}

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(seconds: 4), () {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => HomeScreen(),
          ),
          ModalRoute.withName("/HomeScreen"));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: new Container(
          alignment: Alignment.topCenter,
          child: Text("Splash",
              style:
                  TextStyle(decoration: TextDecoration.none, fontSize: 40.0)),
          padding: new EdgeInsets.only(
              top: MediaQuery.of(context).size.height * .15),
        ),
      ),
    );
  }
}
