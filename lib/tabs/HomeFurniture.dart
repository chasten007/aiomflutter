import 'package:AllInOneMall/utils/StringUtils.dart';
import 'package:AllInOneMall/utils/widgetHelper.dart';
import 'package:flutter/material.dart';



class HomeFurniture extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeFurnitureState();
  }
}

class HomeFurnitureState extends State<HomeFurniture> {

  @override
  void initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:widgetHelper.gridlist(context,StringUtils.homeDecoreFurniture,StringUtils.homeIcons,StringUtils.homeDecoreFurnitureUrls,StringUtils.topsPlaystoreUrl),
    );


  }

}

