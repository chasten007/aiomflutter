import 'package:AllInOneMall/utils/StringUtils.dart';
import 'package:AllInOneMall/utils/widgetHelper.dart';
import 'package:flutter/material.dart';



class NewsEntertMusic extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NewsEntertMusicState();
  }
}

class NewsEntertMusicState extends State<NewsEntertMusic> {

  @override
  void initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:widgetHelper.gridlist(context,StringUtils.newsNentertainment,StringUtils.newsNentertainmentIcons,StringUtils.newsNentertainmentUrls,StringUtils.topsPlaystoreUrl),
    );


  }

}

