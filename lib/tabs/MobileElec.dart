import 'package:AllInOneMall/utils/StringUtils.dart';
import 'package:AllInOneMall/utils/widgetHelper.dart';
import 'package:flutter/material.dart';



class MobileElec extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MobileElecState();
  }
}

class MobileElecState extends State<MobileElec> {

  @override
  void initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:widgetHelper.gridlist(context,StringUtils.mobElect,StringUtils.mobIcons,StringUtils.mobElectUrl,StringUtils.topsPlaystoreUrl),
    );


  }

}

