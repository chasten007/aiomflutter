import 'package:AllInOneMall/utils/StringUtils.dart';
import 'package:AllInOneMall/utils/widgetHelper.dart';
import 'package:flutter/material.dart';



class TravelFood extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TravelFoodState();
  }
}

class TravelFoodState extends State<TravelFood> {

  @override
  void initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:widgetHelper.gridlist(context,StringUtils.travelNFood,StringUtils.travelNFoodIcons,StringUtils.travelNFoodUrl,StringUtils.topsPlaystoreUrl),
    );


  }

}

