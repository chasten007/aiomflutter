import 'package:AllInOneMall/utils/StringUtils.dart';
import 'package:AllInOneMall/utils/widgetHelper.dart';
import 'package:flutter/material.dart';


class TopApps extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TopAppsState();
  }
}

class TopAppsState extends State<TopApps> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:widgetHelper.gridlist(context,StringUtils.tops,StringUtils.topsImgs,StringUtils.topAppsUrls,StringUtils.topsPlaystoreUrl),
    );
  }
}
