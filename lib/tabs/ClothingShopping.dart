import 'package:AllInOneMall/utils/StringUtils.dart';
import 'package:AllInOneMall/utils/widgetHelper.dart';
import 'package:flutter/material.dart';



class ClothingShopping extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ClothingShoppingState();
  }
}

class ClothingShoppingState extends State<ClothingShopping> {

  @override
  void initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:widgetHelper.gridlist(context,StringUtils.clothingsShopping,StringUtils.clothingIcons,StringUtils.clothingsShoppingUrl,StringUtils.topsPlaystoreUrl),
    );


  }

}

