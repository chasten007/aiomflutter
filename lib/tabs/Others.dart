import 'package:AllInOneMall/utils/StringUtils.dart';
import 'package:AllInOneMall/utils/widgetHelper.dart';
import 'package:flutter/material.dart';



class Others extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return OthersState();
  }
}

class OthersState extends State<Others> {

  @override
  void initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:widgetHelper.gridlist(context,StringUtils.other,StringUtils.otherIcons,StringUtils.otherUrl,StringUtils.topsPlaystoreUrl),
    );


  }

}

