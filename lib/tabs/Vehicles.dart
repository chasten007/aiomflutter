import 'package:AllInOneMall/utils/StringUtils.dart';
import 'package:AllInOneMall/utils/widgetHelper.dart';
import 'package:flutter/material.dart';



class Vehicles extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return VehiclesState();
  }
}

class VehiclesState extends State<Vehicles> {

  @override
  void initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:widgetHelper.gridlist(context,StringUtils.vehicles,StringUtils.vehiclesIcons,StringUtils.vehiclesUrl,StringUtils.topsPlaystoreUrl),
    );


  }

}

