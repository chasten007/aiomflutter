import 'package:AllInOneMall/utils/StringUtils.dart';
import 'package:AllInOneMall/utils/widgetHelper.dart';
import 'package:flutter/material.dart';



class SocialNetwork extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SocialNetworkState();
  }
}

class SocialNetworkState extends State<SocialNetwork> {

  @override
  void initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:widgetHelper.gridlist(context,StringUtils.socialNetwork,StringUtils.socialNetworkIcons,StringUtils.socialNetworkUrls,StringUtils.topsPlaystoreUrl),
    );


  }

}

