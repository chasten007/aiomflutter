import 'package:AllInOneMall/utils/StringUtils.dart';
import 'package:AllInOneMall/utils/widgetHelper.dart';
import 'package:flutter/material.dart';



class Grocery extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return GroceryState();
  }
}

class GroceryState extends State<Grocery> {

  @override
  void initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widgetHelper.gridlist(context,StringUtils.grocery,StringUtils.groceryIcon,StringUtils.groceryUrls,StringUtils.topsPlaystoreUrl),
    );


  }

}

