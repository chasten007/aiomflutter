import 'package:AllInOneMall/utils/StringUtils.dart';
import 'package:AllInOneMall/utils/widgetHelper.dart';
import 'package:flutter/material.dart';



class PaymentUpis extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PaymentUpisState();
  }
}

class PaymentUpisState extends State<PaymentUpis> {

  @override
  void initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:widgetHelper.gridlist(context,StringUtils.paymentUpi,StringUtils.paymentIcon,StringUtils.paymentUpiUrl,StringUtils.topsPlaystoreUrl),
    );

  }

}

