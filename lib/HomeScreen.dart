import 'file:///D:/projects/allinonemall/lib/settings/FavAppList.dart';
import 'package:AllInOneMall/settings/AboutUs.dart';
import 'package:AllInOneMall/settings/ContactUs.dart';
import 'package:AllInOneMall/tabs/Books.dart';
import 'package:AllInOneMall/tabs/ClothingShopping.dart';
import 'package:AllInOneMall/tabs/Grocery.dart';
import 'package:AllInOneMall/tabs/HomeFurniture.dart';
import 'package:AllInOneMall/tabs/MobileElec.dart';
import 'package:AllInOneMall/tabs/NewsEntertMusic.dart';
import 'package:AllInOneMall/tabs/Others.dart';
import 'package:AllInOneMall/tabs/PaymentUpis.dart';
import 'package:AllInOneMall/tabs/SocialNetwork.dart';
import 'package:AllInOneMall/tabs/TopApps.dart';
import 'package:AllInOneMall/tabs/TravelFood.dart';
import 'package:AllInOneMall/tabs/Vehicles.dart';
import 'package:AllInOneMall/utils/StringUtils.dart';

import 'package:flutter/material.dart';

class tabOptions {
  const tabOptions({this.title, this.icon});

  final String title;
  final IconData icon;
}

//  static List<String> categories=["All","Top","Books","ClothingShopping","Grocery","HomeDecore & Furniture",
//    "Mobiles & Electronics","News,Entertainment&Music",
//    "Payments & UPIs","SocialNetwork","Travel &Food","Vehicles","Others"];

const List<tabOptions> tabs = const <tabOptions>[
  const tabOptions(title: 'Top', icon: Icons.trending_up),
  const tabOptions(title: 'Books', icon: Icons.import_contacts),
  const tabOptions(title: 'ClothingShopping', icon: Icons.shopping_basket),
  const tabOptions(title: 'Grocery', icon: Icons.local_grocery_store),
  const tabOptions(title: 'HomeDecore & Furniture', icon: Icons.home),
  const tabOptions(title: 'Mobiles & Electronics', icon: Icons.devices_other),
  const tabOptions(
      title: 'News,Entertainment&Music', icon: Icons.music_video),
  const tabOptions(title: 'Payments & UPIs', icon: Icons.payment),
  const tabOptions(title: 'SocialNetwork', icon: Icons.directions_car),
  const tabOptions(title: 'Travel &Food', icon: Icons.directions_car),
  const tabOptions(title: 'Vehicles', icon: Icons.directions_car),
  const tabOptions(title: 'Others', icon: Icons.directions_car),
];

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen> {
  // The app's "state".

  static const List<String> choices = <String>[
    StringUtils.aboutus,
    StringUtils.contactus
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: tabs.length,
        child: Scaffold(
          appBar: AppBar(
            title: Text(StringUtils.appName),
            actions: <Widget>[
              // action button// overflow menu
              IconButton(icon:Icon(Icons.shopping_basket),
                onPressed: (){
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => FavAppList(),
                      ));
                },
              ),
              PopupMenuButton<String>(
                onSelected: choiceACTion,
                itemBuilder: (BuildContext ctx) {
                  return choices.map((String choice) {
                    return PopupMenuItem<String>(
                      value: choice,
                      child: Text(choice),
                    );
                  }).toList();
                },
              ),

            ],
            bottom: TabBar(
              isScrollable: true,
              unselectedLabelColor: Colors.white54,
              indicatorColor: Colors.cyanAccent,
              labelColor: Colors.white,
              tabs: tabs.map((tabOptions index) {
                return Tab(
                  text: index.title,
                  icon: Icon(index.icon),
                );
              }).toList(),
            ),
          ),
          body: Container(

            padding: EdgeInsets.only(top:20.0,right: 10.0,left: 10.0,bottom: 10.0),
            child: TabBarView(
              children: <Widget>[
                TopApps(),
                Books(),
                ClothingShopping(),
                Grocery(),
                HomeFurniture(),
                MobileElec(),
                NewsEntertMusic(),
                PaymentUpis(),
                SocialNetwork(),
                TravelFood(),
                Vehicles(),
                Others(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void choiceACTion(String txt) {
    if (txt == StringUtils.aboutus) {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => AboutUs(),
          ));
    }
    if (txt == StringUtils.contactus) {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ContactUs(),
          ));
    }
  }
}
